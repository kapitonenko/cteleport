﻿namespace DistanceService.Models
{
    public class Airport
    {
        public string IATA { get; set; }
        public string Country { get; set; }
        public Coord Location { get; set; }
    }
}
