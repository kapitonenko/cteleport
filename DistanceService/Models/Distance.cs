﻿using System;

namespace DistanceService.Models
{
    public struct Distance : IEquatable<Distance>
    {
        private const double MeterInMiles = 0.000621371;
        private const double MeterInKm = 0.001;

        private double _m;

        public double Km => _m * MeterInKm;
        public double Miles => _m * MeterInMiles;

        public bool Equals(Distance other) => other._m.Equals(_m);
        public override bool Equals(object obj) => obj is Distance other && Equals(other);
        public override int GetHashCode() => _m.GetHashCode();
        public override string ToString() => $"{Km} km";

        public static Distance FromMeters(double m) => new Distance { _m = m };
        public static Distance FromKm(double km) => new Distance { _m = km / MeterInKm };
        public static Distance FromMiles(double miles) => new Distance { _m = miles / MeterInMiles };

        public static Distance operator +(Distance x, Distance y) => new Distance { _m = x._m + y._m };
        public static Distance operator -(Distance x, Distance y) => new Distance { _m = x._m - y._m };
        public static Distance operator *(Distance x, double f) => new Distance { _m = x._m * f };
        public static Distance operator *(double f, Distance x) => new Distance { _m = x._m * f };
        public static Distance operator /(Distance x, double f) => new Distance { _m = x._m / f };
        public static double operator /(Distance x, Distance y) => x._m / y._m;

        public static bool operator ==(Distance x, Distance y) => x._m == y._m;
        public static bool operator !=(Distance x, Distance y) => x._m != y._m;
        public static bool operator <(Distance x, Distance y) => x._m < y._m;
        public static bool operator <=(Distance x, Distance y) => x._m <= y._m;
        public static bool operator >(Distance x, Distance y) => x._m > y._m;
        public static bool operator >=(Distance x, Distance y) => x._m >= y._m;
    }
}
