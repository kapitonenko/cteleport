﻿using System;

namespace DistanceService.Models
{
    public struct Coord : IEquatable<Coord>
    {
        private const double DegreesinRadian = 180 / Math.PI;

        private double _latRad;
        private double _lonRad;

        public double Lat
        {
            get => _latRad * DegreesinRadian;
            set => _latRad = value / DegreesinRadian;
        }

        public double Lon
        {
            get => _lonRad * DegreesinRadian;
            set => _lonRad = value / DegreesinRadian;
        }

        public Distance DistanceTo(Coord other)
        {
            var dLat = other._latRad - _latRad;
            var dLon = other._lonRad - _lonRad;
            var a = SquareSinusOfHalfAngle(dLat) + Math.Cos(_latRad) * Math.Cos(other._latRad) * SquareSinusOfHalfAngle(dLon);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return c * EarthRadius;
        }

        public bool Equals(Coord other) => other._lonRad.Equals(_lonRad) && other._latRad.Equals(_latRad);
        public override bool Equals(object obj) => obj is Coord other && Equals(other);
        public override int GetHashCode() => _lonRad.GetHashCode() ^ _latRad.GetHashCode();
        public override string ToString() => $"{Lat}, {Lon}";

        private Distance EarthRadius => Distance.FromKm(6371);

        private double SquareSinusOfHalfAngle(double angle)
        {
            var sin = Math.Sin(angle / 2);
            return sin * sin;
        }
    }
}
