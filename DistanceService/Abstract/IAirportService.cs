﻿using System.Threading.Tasks;
using DistanceService.Models;

namespace DistanceService.Abstract
{
    public interface IAirportService
    {
        Task<Airport> GetAirport(string iata);
    }
}