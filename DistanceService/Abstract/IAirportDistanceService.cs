﻿using System.Threading.Tasks;
using DistanceService.Models;

namespace DistanceService.Abstract
{
    public interface IAirportDistanceService
    {
        Task<Distance> GetDistanceBetween(string iata1, string iata2);
    }
}