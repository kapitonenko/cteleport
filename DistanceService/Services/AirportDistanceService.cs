﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DistanceService.Abstract;
using DistanceService.Models;

namespace DistanceService.Services
{
    public class AirportDistanceService : IAirportDistanceService
    {
        private readonly IAirportService _airportService;

        public AirportDistanceService(IAirportService airportService)
        {
            _airportService = airportService;
        }

        public async Task<Distance> GetDistanceBetween(string iata1, string iata2)
        {
            Airport[] airports = await Task.WhenAll(_airportService.GetAirport(iata1), _airportService.GetAirport(iata2));
            return airports[0].Location.DistanceTo(airports[1].Location);
        }
    }
}
