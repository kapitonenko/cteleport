﻿using System.Threading.Tasks;
using DalSoft.RestClient.DependencyInjection;
using DistanceService.Abstract;
using DistanceService.Models;

namespace DistanceService.Services
{
    public class AirportService : IAirportService
    {
        private readonly dynamic _restClient;

        public AirportService(IRestClientFactory restClientFactory)
        {
            _restClient = restClientFactory.CreateClient();
        }

        public async Task<Airport> GetAirport(string iata)
        {
            var airport = await _restClient.Airports.Get(iata);
            airport.HttpResponseMessage.EnsureSuccessStatusCode();
            return airport;
        }
    }
}
