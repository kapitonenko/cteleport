﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace DistanceService.Middleware
{
    public static class ExceptionMiddleware
    {
        public static Task HandleErrorAsync(HttpContext context)
        {
            var errorData = context.Features.Get<IExceptionHandlerPathFeature>();
            context.Response.ContentType = "text/plain";

            var msg = string.Join('\n', errorData.Error.Message, "Request: " + errorData.Path);
            return context.Response.WriteAsync(msg);
        }
    }
}
