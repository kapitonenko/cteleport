﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DistanceService.Abstract;
using DistanceService.Services;
using Microsoft.AspNetCore.Mvc;

namespace DistanceService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirportController : ControllerBase
    {
        private readonly IAirportDistanceService _airportDistanceService;

        public AirportController(IAirportDistanceService airportDistanceService)
        {
            _airportDistanceService = airportDistanceService;
        }

        [HttpGet("distance/{iata1}-{iata2}")]
        public async Task<ActionResult<double>> GetDistanceBetween(string iata1, string iata2)
        {
            var distance = await _airportDistanceService.GetDistanceBetween(iata1, iata2);
            return distance.Miles;
        }
    }
}
