﻿using DalSoft.RestClient.DependencyInjection;
using DistanceService.Abstract;
using DistanceService.Config;
using DistanceService.Middleware;
using DistanceService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DistanceService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var mainOptions = new MainOptions();
            Configuration.Bind(mainOptions);
            services.AddRestClient(mainOptions.SourceUrl);

            services
                .AddTransient<IAirportService, AirportService>()
                .AddTransient<IAirportDistanceService, AirportDistanceService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseExceptionHandler(action => action.Run(ExceptionMiddleware.HandleErrorAsync));
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
