﻿using DistanceService.Models;
using Xunit;

namespace DistanceService.UnitTests
{
    public class CoordTest
    {
        [Theory]
        [InlineData(0, 0, 0, 1, 110, 112)]
        [InlineData(0, 100, 0, 99, 110, 112)]
        [InlineData(0, 0, 1, 0, 110, 112)]
        [InlineData(0, 0, -1, 0, 110, 112)]
        [InlineData(40, 0, 50, 0, 1100, 1120)]
        [InlineData(60, 0, 60, 1, 55, 56)]
        [InlineData(60, 100, 60, 102, 110, 112)]
        [InlineData(75, 100, 75, 99, 28, 30)]
        public void DistanceTo(double lat1, double lon1, double lat2, double lon2, double minKm, double maxKm)
        {
            // Arrange
            var coord1 = new Coord { Lat = lat1, Lon = lon1 };
            var coord2 = new Coord { Lat = lat2, Lon = lon2 };
            var minDistance = Distance.FromKm(minKm);
            var maxDistance = Distance.FromKm(maxKm);

            // Act 
            var distance = coord1.DistanceTo(coord2);

            // 3. Assert 
            Assert.True(distance >= minDistance, $"Distance is less then expected: {distance} < {minDistance}");
            Assert.True(distance <= maxDistance, $"Distance is bigger then expected: {distance} > {maxDistance}");
        }
    }
}
