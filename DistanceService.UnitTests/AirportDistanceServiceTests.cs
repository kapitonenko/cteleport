using System.Threading.Tasks;
using DistanceService.Abstract;
using DistanceService.Models;
using DistanceService.Services;
using Moq;
using Xunit;

namespace DistanceService.UnitTests
{
    public class AirportDistanceServiceTests
    {
        [Fact]
        public async void GetDistanceBetween()
        {
            // Arrange
            Airport[] airports = {
                new Airport
                {
                    IATA = "AMS",
                    Location = new Coord
                    {
                        Lon = 4.763385,
                        Lat = 52.309069
                    }
                },
                new Airport
                {
                    IATA = "BRU",
                    Location = new Coord
                    {
                        Lon = 4.451703,
                        Lat = 50.456697
                    }
                }
            };
            var airportServiceMock = new Mock<IAirportService>();
            foreach (var airport in airports)
            {
                airportServiceMock.Setup(s => s.GetAirport(airport.IATA)).Returns(Task.FromResult(airport));
            }
            var airportDistanceService = new AirportDistanceService(airportServiceMock.Object);

            // Act 
            var distance = await airportDistanceService.GetDistanceBetween(airports[0].IATA, airports[1].IATA);

            // 3. Assert 
            Assert.Equal(airports[0].Location.DistanceTo(airports[1].Location), distance);
        }
    }
}
